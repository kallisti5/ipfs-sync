FROM fedora:latest

ENV PATH=$PATH:/usr/local/bin

RUN dnf install -y rsync bash wget \
	&& cd /tmp && wget https://github.com/ipfs/go-ipfs/releases/download/v0.6.0/go-ipfs_v0.6.0_linux-amd64.tar.gz \
	&& tar xvf go-ipfs_v0.6.0_linux-amd64.tar.gz && mv go-ipfs/ipfs /usr/local/bin/ && chmod 755 /usr/local/bin/ipfs \
	&& rm -rf /tmp/go-ipfs

ADD ipfs-sync /usr/local/bin/ipfs-sync
RUN chmod 755 /usr/local/bin/ipfs-sync

VOLUME /cache

ENTRYPOINT ["/usr/local/bin/ipfs-sync"]
