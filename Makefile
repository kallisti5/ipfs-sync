default:
	docker build . -t docker.io/terarocket/ipfs-mirror
test:
	docker run -t -v ipfs-mirror-cache:/cache --env-file=$(ENVFILE) docker.io/terarocket/ipfs-mirror
enter:
	docker run -t -v ipfs-mirror-cache:/cache docker.io/terarocket/ipfs-mirror  /bin/bash -l
