# ipfs-sync

This tool will sync a set of files on various medium to a server/cluster of IPFS nodes.

## Persistent Volumes

* Cache ```/cache```
  * This volume should be either the files you want to push to IPFS, or a persistent scratch directory ipfs-sync can rsync into.

## Known Issues

* Reuploading the same data to IPFS can take quite some time on every execution.
  * ipfs transfers the entire file to the remote API server before validating if the server already has it. 
  * https://github.com/ipfs/go-ipfs/issues/7586

## Environment

### Required

* IPFS_API_SERVER
  * IPFS Server Location (quic address)
  * Example: ```IPFS_API_SERVER=/ip4/192.168.1.10/tcp/45001```
* SOURCE_LOCATION
  * Source location of files.
  * Example: ```SOURCE_LOCATION=local:///cache``` (data attached to container at this path)
  * Example: ```SOURCE_LOCATION=rsync://rsync.haiku-os.org:12000/haikuports-master``` (rsync data to /cache)

### Optional

* IPNS_KEY
  * Public key to update to new ipfs hash.
  * Example: ```IPNS_KEY=Qm.......```

## Testing

> Put your desired configuration into a file called myenvfile.

```ENVFILE=myenvfile make test```
